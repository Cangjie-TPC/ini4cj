<div align="center">
<h1>ini4cj</h1>
</div>

<p align="center">
<img alt="" src="https://img.shields.io/badge/release-v1.0.1-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/build-pass-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjc-v0.58.3-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjcov-92.4%25-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/project-open-brightgreen" style="display: inline-block;" />
</p>


## <img alt="" src="./doc/assets/readme-icon-introduction.png" style="display: inline-block;" width=3%/> 简介

INI 文件是一种无固定标准格式的配置文件。它以简单的文字与简单的结构组成，常常使用在 Windows 操作系统上，许多程序也会采用 INI 文件做为配置文件使用。Windows 操作系统后来以注册表的形式取代了 INI 文件。INI 文件的命名来源于英文”初始（Initial）“的前三个字符，正与它的用途 —— 初始化程序相对应。

### 特性

- 🚀 简洁且归一化的接口
- 🚀 支持字符串、整数、长整数、浮点、布尔
- 💪 整数溢出检查
- :headphones: 支持多行
- :earth_asia: 输出 DataModel（待实现）

### 路线

<p align="center">
<img src="./doc/assets/milestone.png" width="100%" >
</p>

## <img alt="" src="./doc/assets/readme-icon-framework.png" style="display: inline-block;" width=3%/> 架构

### 架构图：

<p align="center">
<img src="./doc/assets/framework.png" width="100%" >
</p>

- section 解析
- 左值 key 解析
- 右值 value 解析
- 输出 DataModel

### 源码目录

```shell
.
├── doc
│   ├── assets
│   ├── cjcov
│   ├── design.md
│   └── feature_api.md
├── README.md
├── src
│   ├── IniDataModel.cj
│   ├── IniParser.cj
│   └── IniValue.cj
└── test
    ├── HLT
    ├── LLT
    └── UT
```

- `doc` 存放库的设计文档、提案、库的使用文档、LLT 用例覆盖率
- `src` 库源码目录
- `test` 存放测试用例，包括 HLT 用例、LLT 用例和 UT 用例

### 接口说明

详情见[API](./doc/feature_api.md)

## <img alt="" src="./doc/assets/readme-icon-compile.png" style="display: inline-block;" width=3%/> 使用说明

### 编译

```shell
cjpm build
```

### 功能示例

**ini文件：**

```ini
[section1]
aval=false
bval=-123456789
cval=10
dval=1111.111111
eval=-123456.7890
fval="hello world"

[section2]
aval=false
bval=-123456789
cval=10
dval=1111.111111
eval=-123456.7890
fval="hello world"

[section3]
aval=false
bval=-123456789
cval=10
dval=1111.111111
eval=-123456.7890
fval="hello world"
```

**parse 接口调用：**

将下面测试文件main.cj放到src下。

```cangjie

import ini4cj.*
main(): Unit {
     let parser:IniParser = IniParser("test_multiple_sections_001.ini")
     let ini:IniFile = parser.parse()
     println(ini.toString())
}
```

**运行结果：**

```shell
[section1]
aval=false
bval=-123456789
cval=10
dval=1111.111111
eval=-123456.7890
fval="hello world"


[section2]
aval=false
bval=-123456789
cval=10
dval=1111.111111
eval=-123456.7890
fval="hello world"


[section3]
aval=false
bval=-123456789
cval=10
dval=1111.111111
eval=-123456.7890
fval="hello world"
jval=9111111111111111110
```

**获取 value 接口调用：**

将下面测试文件main.cj放到src下。

```cangjie

import ini4cj.*
main(): Unit {
     let parser:IniParser = IniParser("test_multiple_sections_001.ini")
     let ini:IniFile = parser.parse()

     let aval: IniValue = ini.get("section1").get("aval")
     let bval: IniValue = ini.get("section1").get("bval")
     let cval: IniValue = ini.get("section1").get("cval")
     let dval: IniValue = ini.get("section1").get("dval")
     let eval: IniValue = ini.get("section1").get("eval")
     let fval: IniValue = ini.get("section1").get("fval")
     let a: Bool = parser.getBoolean(aval)
     let b: Int32 = parser.getInt(bval)
     let c: Int32 = parser.getInt(cval)
     let d: Float64 = parser.getDouble(dval)
     let e: Float64 = parser.getDouble(eval)
     let f: String = parser.getString(fval)
     println("a=${a}")
     println("b=${b}")
     println("c=${c}")
     println("d=${d}")
     println("e=${e}")
     println("f=${f}")
}
```

**运行结果：**

```shell
a=false
b=-123456789
c=10
d=1111.111111
e=-123456.789000
f="hello world"
```
## 约束与限制

在下述版本验证通过：

    Cangjie Version: 0.58.3

## 开源协议

本项目基于 [木兰宽松许可证](./LICENSE)，请自由的享受和参与开源。

## 参与贡献

欢迎给我们提交PR，欢迎给我们提交Issue，欢迎参与任何形式的贡献。
