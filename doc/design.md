### 三方库设计说明

#### 1 需求场景分析

INI 文件是一种无固定标准格式的配置文件。它以简单的文字与简单的结构组成，常常使用在 Windows 操作系统上，许多程序也会采用 INI 文件做为配置文件使用。Windows 操作系统后来以注册表的形式取代了 INI 文件。INI 文件的命名来源于英文”初始（Initial）“的前三个字符，正与它的用途 —— 初始化程序相对应。

#### 2 三方库对外提供的特性

（1） 简洁且归一化的接口

（2） 支持字符串、整数、长整数、浮点、布尔

（3） 整数溢出检查

（4） 支持多行

（5） 输出 DataModel（待实现）

#### 3 License分析

无

#### 4 依赖分析 

from std import collection.*
from std import io.*
from std import convert.*
from std import math.*

#### 5 特性设计文档

##### 5.1 核心特性1 

###### 5.1.1 特性介绍

    定义 ini 各种类型类

###### 5.1.2 实现方案

    初始化构造，获取对应值。

###### 5.1.3 接口设计

💡 IniString 

| 成员函数 | 入参 | 返回值 | 作用描述 |
| --- | --- |  --- | --- |
| init   | val: String | / | 初始化 IniString |
| getValue   | / | String | 获取 value 值 |
| toString   | / | String | 将 IniString 转为 String |
| toIniString   | / | String | 将 IniString 转为 IniString |

💡 IniInt 

| 成员函数 | 入参 | 返回值 | 作用描述 |
| --- | --- |  --- | --- |
| init   | val: Int32 | / | 初始化 IniInt |
| getValue   | / | Int32 | 获取 value 值 |
| toString   | / | String | 将 IniInt 转为 String |
| toIniString   | / | String | 将 IniInt 转为 IniString |

💡 IniLongInt 

| 成员函数 | 入参 | 返回值 | 作用描述 |
| --- | --- |  --- | --- |
| init   | val: Int64 | / | 初始化 IniLongInt |
| getValue   | / | Int64 | 获取 value 值 |
| toString   | / | String | 将 IniLongInt 转为 String |
| toIniString   | / | String | 将 IniLongInt 转为 IniString |

💡 IniDouble 

ini 只支持 normal 浮点表示

| 成员函数 | 入参 | 返回值 | 作用描述 |
| --- | --- |  --- | --- |
| init   | val: Float64 | / | 初始化 IniDouble |
| getValue   | / | Float64 | 获取 value 值 |
| toString   | / | String | 将 IniDouble 转为 String |
| toIniString   | / | String | 将 IniDouble 转为 IniString |

💡 IniBoolean 

| 成员函数 | 入参 | 返回值 | 作用描述 |
| --- | --- |  --- | --- |
| init   | val: Bool | / | 初始化 IniBoolean |
| getValue   | / | Bool | 获取 value 值 |
| toString   | / | String | 将 IniBoolean 转为 String |
| toIniString   | / | String | 将 IniBoolean 转为 IniString |


###### 5.1.4 展示示例

```cangjie
// DEPENDENCE: test_multiple_sections_001.ini
// EXEC: cjc %import-path %L %l %f
// EXEC: ./main

from ini4cj import ini.*
main(): Unit {
     let parser:IniParser = IniParser("test_multiple_sections_001.ini")
     let ini:IniFile = parser.parse()

     let aval: IniValue = ini.get("section1").get("aval")
     let bval: IniValue = ini.get("section1").get("bval")
     let cval: IniValue = ini.get("section1").get("cval")
     let dval: IniValue = ini.get("section1").get("dval")
     let eval: IniValue = ini.get("section1").get("eval")
     let fval: IniValue = ini.get("section1").get("fval")
     let a: Bool = parser.getBoolean(aval)
     let b: Int32 = parser.getInt(bval)
     let c: Int32 = parser.getInt(cval)
     let d: Float64 = parser.getDouble(dval)
     let e: Float64 = parser.getDouble(eval)
     let f: String = parser.getString(fval)
     println("a=${a}")
     println("b=${b}")
     println("c=${c}")
     println("d=${d}")
     println("e=${e}")
     println("f=${f}")
}
```

**运行结果：**

```shell
a=false
b=-123456789
c=10
d=1111.111111
e=-123456.789000
f=hello world
```

##### 5.2 核心特性2

###### 5.2.1 特性介绍

    ini 解析器

###### 5.2.2 实现方案

    ini -> section -> filed -> left value/right value

###### 5.2.3 接口设计

💡 IniField 

| 成员函数 | 入参 | 返回值 | 作用描述 |
| --- | --- |  --- | --- |
| init   | ky: String<br> val: IniValue | / | 初始化 IniField |
| toString   | / | String | 将 IniField 转为 String |

💡 IniSection  

| 成员函数 | 入参 | 返回值 | 作用描述 |
| --- | --- |  --- | --- |
| init   | name: String | / | 初始化 IniSection |
| init   | / | / | 初始化 IniSection |
| toString   | / | String | 将 IniSection 转为 String |
| get  | key: String | IniValue | 获取 key 值对应的 IniValue |

💡 IniFile   

| 成员函数 | 入参 | 返回值 | 作用描述 |
| --- | --- |  --- | --- |
| init   | name: String | / | 初始化 IniFile |
| init   | / | / | 初始化 IniSection |
| toString   | / | String | 将 IniFile 转为 String |
| get  | key: String | IniSection | 获取 key 对应的 IniSection 值 |

💡 IniParser   

| 成员函数 | 入参 | 返回值 | 作用描述 |
| --- | --- |  --- | --- |
| init   | pathName: String | / | 初始化 IniParser |
| parse   | / | IniFile | 文件解析 |
| getString   | val: IniValue | String | 获取 IniValue 对应的 String |
| getInt  | val: IniValue | Int32 | 获取 IniValue 对应的 Int32 |
| getLongInt  | val: IniValue | Int64 | 获取 IniValue 对应的 Int64 |
| getDouble  | val: IniValue | Float64 | 获取 IniValue 对应的 Float64 |
| getBoolean  | val: IniValue | Bool | 获取 IniValue 对应的 Bool |

###### 5.2.4 展示示例

```cangjie
// DEPENDENCE: test_multiple_sections_001.ini
// EXEC: cjc %import-path %L %l %f
// EXEC: ./main

from ini4cj import ini.*
main(): Unit {
     let parser:IniParser = IniParser("test_multiple_sections_001.ini")
     let ini:IniFile = parser.parse()
     println(ini.toString())
}
```

**运行结果：**

```
[section1]
aval=false
bval=-123456789
cval=10
dval=1111.111111
eval=-123456.7890
fval="hello world"


[section2]
aval=false
bval=-123456789
cval=10
dval=1111.111111
eval=-123456.7890
fval="hello world"


[section3]
aval=false
bval=-123456789
cval=10
dval=1111.111111
eval=-123456.7890
fval="hello world"
jval=9111111111111111110
```