# ini4cj 库

### 介绍

​		 INI 文件是一种无固定标准格式的配置文件。

### 1 读取并解析ini 文件

场景：

   1. 读取并解析ini 文件
   2. 整数溢出检查
      约束：只支持字符串、整数、长整数、浮点、布尔
      性能：支持版本几何性能持平
      可靠性： NA

#### 1.1 读取并解析ini 文件

 读取ini文件

#### 1.1.1 主要接口

```cangjie
public class IniParser {

    /*
     * 初始化解析器
     *
     * 参数 pathName - Fil文件路径
     */
    public init(pathName: String) 

    /*
     * 文件解析
     * ini -> section -> filed -> left value/right value
     *
     * 返回值 IniFile - 返回解析后的 IniFile
     */
    public func parse(): IniFile

    /*
     * 获取 IniValue 对应的 String
     *
     * 参数 val - IniValue 
     *
     * 返回值 String - 返回转化后的 String 值
     */
    public func getString(val: IniValue): String

    /*
     * 获取 Int32 值
     *
     * 参数 val - IniValue
     *
     * 返回值 Int32 - 返回转化后的 Int32 值
     */
    public func getInt(val: IniValue): Int32

    /*
     * 获取 Int64 值
     *
     * 参数 val - IniValue
     *
     * 返回值 Int64 - 返回转化后的 Int64 值
     */
    public func getLongInt(val: IniValue): Int64

    /*
     * 获取 Float64 值
     *
     * 参数 val - IniValue
     *
     * 返回值 Float64 - 返回转化后的 Float64 值
     */
    public func getDouble(val: IniValue): Float64

    /*
     * 获取 Bool 值
     *
     * 参数 val - IniValue
     *
     * 返回值 Bool - 返回转化后的 Bool 值
     */
    public func getBoolean(val: IniValue): Bool
}
```

```cangjie
public class IniField {

    /*
     * 初始化IniField
     *
     * 参数 ky - key String
     * 参数 val - value IniValue
     */
    public init(ky: String, val: IniValue)

   
    /*
     * 对象转换为String格式
     *
     * 返回值 String - 对象转换为String格式
     */
    public func toString(): String

}
```

```cangjie
public class IniSection {

    /*
     * 初始化
     *
     * 参数 name - sectionName
     */
    public init(name: String)

    /*
     * 初始化 默认sectionName = "default"
     *
     */
    public init()

    /*
     * 打印sectionName 和 遍历fields后的值
     *
     * 返回值 String - 对象转换为String格式
     */
    public func toString(): String

    /*
     * get IniValue value
     *
     * 参数 key - the key of fields
     * 返回值 IniValue - 根据fields的key查询到的IniValue
     */
    public func get(key: String): IniValue

}
```

```cangjie
public class IniFile {

    /*
     * 初始化
     *
     * 参数 name - iniName
     */
    public init(name: String)

    /*
     * 初始化 默认iniName = "default"
     *
     */
    public init()

    /*
     * 打印iniName 和 遍历sections后的值
     *
     * 返回值 String - iniName 和 遍历sections后的值
    public func toString(): String

    /*
     * get IniSection value
     *
     * 参数 key - the key of fields
     * 返回值 IniSection - 根据fields的key查询到的IniSection
     */
    public func get(key: String): IniSection

}
```

```cangjie
public class IniException {

    /*
     * 初始化
     *
     */
    public init()

    /*
     * 初始化异常信息
     *
     * 参数 messages - 异常信息
     *
     */
    public init(messages: String)

    /*
     * 获取异常报错信息
     *
     * 返回值 String - 异常报错信息
    public func getMessage(): String

    /*
     * 打印异常信息
     *
     * 返回值 String - 异常信息
     */
    public override func toString(): String

}
```

```cangjie
public enum IniType {
    IniTypeString | IniTypeInt | IniTypeLongInt | IniTypeDouble | IniTypeBoolean
}
```

```cangjie
public abstract class IniValue {

    /*
     * IniValue对象转换为String类型
     *
     * 返回值 String - IniValue对象转换为String类型
     */
    public func toString(): String

    /*
     * IniValue对象转换为String类型
     *
     * 返回值 String - IniValue对象转换为String类型
     */
    public func toIniString(): String

}
```


```cangjie
public class IniString {

    /*
     * 初始化IniString
     *
     * 参数 val - String值
     */
    public init(val: String)

    /*
     * 返回String格式的value
     *
     * 返回值 String - 返回String类型的value
     */
    public func getValue(): String

    /*
     * IniString对象转换为String类型
     *
     * 返回值 String - IniString对象转换为String类型
     */
    public func toString(): String

    /*
     * IniString对象转换为String类型
     *
     * 返回值 String - IniString对象转换为String类型
     */
    public func toIniString(): String

}
```

```cangjie
public class IniInt {

    /*
     * 初始化IniInt
     *
     * 参数 val - Int32值
     */
    public init(val: Int32)

    /*
     * 返回Int32格式的value
     *
     * 返回值 Int32 - 返回Int32类型的value
     */
    public func getValue(): Int32

    /*
     * IniInt对象转换为String类型
     *
     * 返回值 String - IniInt对象转换为String类型
     */
    public func toString(): String

    /*
     * IniInt对象转换为String类型
     *
     * 返回值 String - IniInt对象转换为String类型
     */
    public func toIniString(): String

}
```

```cangjie
public class IniLongInt {

    /*
     * 初始化IniLongInt
     *
     * 参数 val - Int64值
     */
    public init(val: Int64)

    /*
     * 返回Int64格式的value
     *
     * 返回值 Int64 - 返回Int64类型的value
     */
    public func getValue(): Int64

    /*
     * IniLongInt对象转换为String类型
     *
     * 返回值 String - IniLongInt对象转换为String类型
     */
    public func toString(): String

    /*
     * IniLongInt对象转换为String类型
     *
     * 返回值 String - IniLongInt对象转换为String类型
     */
    public func toIniString(): String

}
```

```cangjie
public class IniDouble {

    /*
     * 初始化IniDouble
     *
     * 参数 val - Float64值
     */
    public init(val: Float64)

    /*
     * 返回Float64格式的value
     *
     * 返回值 Float64 - 返回Float64类型的value
     */
    public func getValue(): Float64

    /*
     * IniDouble对象转换为String类型
     *
     * 返回值 String - IniDouble对象转换为String类型
     */
    public func toString(): String

    /*
     * IniDouble对象转换为String类型
     *
     * 返回值 String - IniDouble对象转换为String类型
     */
    public func toIniString(): String

}
```

```cangjie
public class IniBoolean {

    /*
     * 初始化IniBoolean
     *
     * 参数 val - Bool值
     */
    public init(val: Bool)

    /*
     * 返回Bool格式的value
     *
     * 返回值 Bool - 返回Bool类型的value
     */
    public func getValue(): Bool

    /*
     * IniBoolean对象转换为String类型
     *
     * 返回值 String - IniBoolean对象转换为String类型
     */
    public func toString(): String

    /*
     * IniBoolean对象转换为String类型
     *
     * 返回值 String - IniBoolean对象转换为String类型
     */
    public func toIniString(): String

}
```

#### 1.1.2示例

test_multiple_sections_001.ini文件

```
[section1]
aval=false
bval=-123456789
cval=10
dval=1111.111111
eval=-123456.7890
fval="hello world"

[section2]
aval=false
bval=-123456789
cval=10
dval=1111.111111
eval=-123456.7890
fval="hello world"
```

```
// DEPENDENCE: test_multiple_sections_001.ini
// EXEC: cjc %import-path %L %l %f
// EXEC: ./main

import ini4cj.*
main(): Unit {
     let parser:IniParser = IniParser("test_multiple_sections_001.ini")
     let ini:IniFile = parser.parse()

     let aval: IniValue = ini.get("section1").get("aval")
     let bval: IniValue = ini.get("section1").get("bval")
     let cval: IniValue = ini.get("section1").get("cval")
     let dval: IniValue = ini.get("section1").get("dval")
     let eval: IniValue = ini.get("section1").get("eval")
     let fval: IniValue = ini.get("section1").get("fval")
     let a: Bool = parser.getBoolean(aval)
     let b: Int32 = parser.getInt(bval)
     let c: Int32 = parser.getInt(cval)
     let d: Float64 = parser.getDouble(dval)
     let e: Float64 = parser.getDouble(eval)
     let f: String = parser.getString(fval)
     println("a=${a}")
     println("b=${b}")
     println("c=${c}")
     println("d=${d}")
     println("e=${e}")
     println("f=${f}")
}
```

**运行结果：**

```
a=false
b=-123456789
c=10
d=1111.111111
e=-123456.789000
f="hello world"
```